package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size for #id"() {
        when:
        def userId = new UserId(id)

        then:
        userId.correctSize == expectedResult

        where:
        id                 | expectedResult
        ''                 | false
        '123'              | false
        '25405823451'      | true
        '2872472398749812' | false
    }

    @Unroll
    def "should check sex for #id"() {
        when:
        def userId = new UserId(id)

        then:
        userId.sex.orElse(null) == expectedSex

        where:
        id            | expectedSex
        ''            | null
        '523'         | null
        '56032341811' | UserIdChecker.Sex.MAN
        '56032355504' | UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should check correctness for #id"() {
        when:
        def userId = new UserId(id)

        then:
        userId.isCorrect() == expectedResult

        where:
        id            | expectedResult
        ''            | false
        '12312'       | false
        '56032355504' | true
        '56032341811' | true
        '95101578331' | false
        'abdtujnopff' | false
    }

    @Unroll
    def "should check date for #id"() {
        when:
        def userId = new UserId(id)

        then:
        userId.date.orElse(null) == expectedDate

        where:
        id            | expectedDate
        '83101878369' | '18-10-1983'
        '76030146492' | '01-03-1976'
        '95101578331' | null
        'abdtujnopff' | null
    }
}
