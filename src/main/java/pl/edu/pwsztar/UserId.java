package pl.edu.pwsztar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

final class UserId implements UserIdChecker {

    static final int USER_ID_LENGTH = 11;

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == USER_ID_LENGTH;
    }

    @Override
    public Optional<Sex> getSex() {
        if (isCorrect()) {
            if (Character.getNumericValue(id.charAt(9)) % 2 == 1) {
                return Optional.of(Sex.MAN);
            } else {
                return Optional.of(Sex.WOMAN);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean isCorrect() {
        if (!isCorrectSize()) {
            return false;
        }
        if (!isNumeric()) {
            return false;
        }
        return checkSum();
    }

    @Override
    public Optional<String> getDate() {
        if (!isCorrect()) {
            return Optional.empty();
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.of(getBirthYear(), getBirthMonth(), getBirthDay());
        return Optional.of(date.format(formatter));
    }

    private boolean isNumeric() {
        return id.matches("[0-9]+");
    }

    private boolean checkSum() {
        final byte[] idBytes = getIdBytes();
        int sum = idBytes[0] + 3 * idBytes[1] + 7 * idBytes[2] + 9 * idBytes[3] + idBytes[4] + 3 * idBytes[5] +
                7 * idBytes[6] + 9 * idBytes[7] + idBytes[8] + 3 * idBytes[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        return sum == idBytes[10];
    }

    public int getBirthYear() {
        final byte[] idBytes = getIdBytes();
        int year;
        int month;
        year = 10 * idBytes[0];
        year += idBytes[1];
        month = 10 * idBytes[2];
        month += idBytes[3];
        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 0 && month < 13) {
            year += 1900;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    public int getBirthMonth() {
        final byte[] idBytes = getIdBytes();
        int month;
        month = 10 * idBytes[2];
        month += idBytes[3];
        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    public int getBirthDay() {
        final byte[] idBytes = getIdBytes();
        int day;
        day = 10 * idBytes[4];
        day += idBytes[5];
        return day;
    }

    private byte[] getIdBytes() {
        final byte[] idBytes = new byte[11];
        for (int i = 0; i < 11; i++) {
            idBytes[i] = Byte.parseByte(id.substring(i, i + 1));
        }
        return idBytes;
    }
}
